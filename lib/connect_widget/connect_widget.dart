
import 'package:flutter/material.dart';


class ConnectWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 537,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    left: 0,
                    top: 0,
                    right: 5,
                    child: Container(
                      height: 537,
                      decoration: BoxDecoration(
                        color: Color.fromARGB(102, 135, 131, 209),
                        borderRadius: BorderRadius.all(Radius.circular(36)),
                      ),
                      child: Container(),
                    ),
                  ),
                  Positioned(
                    left: 3,
                    top: 147,
                    right: 0,
                    child: Container(
                      height: 390,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(left: 33),
                              child: Text(
                                "Connect",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 36,
                                  fontFamily: "Roboto",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Container(
                            height: 244,
                            margin: EdgeInsets.only(top: 20),
                            child: Image.asset(
                              "assets/images/group-18.png",
                              fit: BoxFit.cover,
                            ),
                          ),
                          Container(
                            height: 24,
                            margin: EdgeInsets.only(left: 93, top: 46, right: 100),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Messages",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 66, 33, 61),
                                      fontSize: 16,
                                      fontFamily: "Roboto",
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Spacer(),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "People",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 66, 33, 61),
                                      fontSize: 16,
                                      fontFamily: "Roboto",
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 51,
                              height: 3,
                              margin: EdgeInsets.only(left: 105, top: 10),
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 255, 255, 255),
                                borderRadius: BorderRadius.all(Radius.circular(1.5)),
                              ),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 340,
                height: 52,
                margin: EdgeInsets.only(top: 44),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        width: 52,
                        height: 52,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(128, 189, 64, 137),
                          borderRadius: BorderRadius.all(Radius.circular(26)),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 17),
                              child: Text(
                                "M",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  fontSize: 20,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 126,
                      height: 41,
                      margin: EdgeInsets.only(left: 16, top: 3),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Mariam",
                              style: TextStyle(
                                color: Color.fromARGB(255, 66, 33, 61),
                                fontSize: 12,
                                fontFamily: "Roboto",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                "Sounds good to me! 😎",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 12,
                                  fontFamily: "Roboto",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                      child: Text(
                        "Fri",
                        style: TextStyle(
                          color: Color.fromARGB(255, 194, 193, 194),
                          fontSize: 12,
                          fontFamily: "Roboto",
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 340,
                height: 52,
                margin: EdgeInsets.only(top: 25),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        width: 52,
                        height: 52,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(153, 135, 131, 209),
                          borderRadius: BorderRadius.all(Radius.circular(26)),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 20, right: 19),
                              child: Text(
                                "P",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  fontSize: 20,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 159,
                      height: 41,
                      margin: EdgeInsets.only(left: 16, top: 3),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Paul",
                              style: TextStyle(
                                color: Color.fromARGB(255, 66, 33, 61),
                                fontSize: 12,
                                fontFamily: "Roboto",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                "It takes courage to do that 💪",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 12,
                                  fontFamily: "Roboto",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                      child: Text(
                        "Fri",
                        style: TextStyle(
                          color: Color.fromARGB(255, 194, 193, 194),
                          fontSize: 12,
                          fontFamily: "Roboto",
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            Container(
              height: 67,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(128, 141, 141, 141),
                    offset: Offset(0, 2),
                    blurRadius: 4,
                  ),
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 54, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 18,
                          child: Text(
                            "HOME",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/home-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 70, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/local-play-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: 21,
                    height: 32,
                    margin: EdgeInsets.only(top: 20, right: 69),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 16,
                          right: 0,
                          child: Text(
                            "CHAT",
                            style: TextStyle(
                              color: Color.fromARGB(255, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 1,
                          child: Container(
                            width: 20,
                            height: 20,
                            child: Image.asset(
                              "assets/images/chat-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 31,
                    height: 30,
                    margin: EdgeInsets.only(top: 22, right: 49),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 14,
                          right: 0,
                          child: Text(
                            "PROFILE",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 9,
                          child: Container(
                            width: 16,
                            height: 16,
                            child: Image.asset(
                              "assets/images/person-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
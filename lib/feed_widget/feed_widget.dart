
import 'package:flutter/material.dart';


class FeedWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 482,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    left: 39,
                    top: 0,
                    right: 40,
                    child: Container(
                      height: 482,
                      decoration: BoxDecoration(
                        color: Color.fromARGB(102, 135, 131, 209),
                        borderRadius: BorderRadius.all(Radius.circular(36)),
                      ),
                      child: Container(),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 92,
                    right: 0,
                    child: Container(
                      height: 323,
                      child: Image.asset(
                        "assets/images/undraw-contemplating-8t0x-2.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    left: 71,
                    top: 94,
                    child: Container(
                      width: 82,
                      height: 96,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 7,
                              height: 12,
                              child: Image.asset(
                                "assets/images/material-icons-white-chevron-left.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(left: 4, top: 41),
                              child: Text(
                                "Feed",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 36,
                                  fontFamily: "Roboto",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 48, top: 37),
                child: Text(
                  "Tips based on your mood analysis",
                  style: TextStyle(
                    color: Color.fromARGB(255, 194, 193, 194),
                    fontSize: 16,
                    fontFamily: "Roboto",
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 349,
                height: 162,
                margin: EdgeInsets.only(top: 42, right: 29),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        width: 349,
                        height: 162,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(110, 165, 105, 149),
                              offset: Offset(0, 1),
                              blurRadius: 4,
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(18)),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 22, top: 22),
                              child: Text(
                                "Meditation",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 16,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              width: 192,
                              margin: EdgeInsets.only(left: 22),
                              child: Text(
                                "Your stress levels have been higher than usual this week, tap to see some meditation tips.",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 194, 193, 194),
                                  fontSize: 16,
                                  fontFamily: "Roboto",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      top: 21,
                      right: 19,
                      child: Container(
                        width: 128,
                        height: 120,
                        child: Image.asset(
                          "assets/images/group-16.png",
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        width: 349,
                        height: 162,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(64, 165, 105, 149),
                              offset: Offset(0, 1),
                              blurRadius: 4,
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(18)),
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              left: 22,
                              top: 22,
                              child: Text(
                                "Meditation",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 16,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Positioned(
                              left: 22,
                              top: 21,
                              right: 19,
                              child: Container(
                                height: 121,
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Positioned(
                                      left: 0,
                                      top: 25,
                                      child: Container(
                                        width: 192,
                                        child: Text(
                                          "Your stress levels have been higher than usual this week, tap to see some meditation tips.",
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 194, 193, 194),
                                            fontSize: 16,
                                            fontFamily: "Roboto",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      right: -0,
                                      child: Container(
                                        width: 128,
                                        height: 120,
                                        child: Image.asset(
                                          "assets/images/group-16.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        width: 349,
                        height: 162,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(64, 165, 105, 149),
                              offset: Offset(0, 1),
                              blurRadius: 4,
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(14)),
                        ),
                        child: Stack(
                          alignment: Alignment.centerRight,
                          children: [
                            Positioned(
                              left: 22,
                              top: 25,
                              child: Container(
                                width: 192,
                                height: 120,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Meditation",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 66, 33, 61),
                                          fontSize: 16,
                                          fontFamily: "Roboto",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Text(
                                      "Your stress levels have been higher than usual this week, tap to see some meditation tips.",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 194, 193, 194),
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              right: 19,
                              child: Container(
                                width: 128,
                                height: 120,
                                child: Image.asset(
                                  "assets/images/group-16-3.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 349,
                height: 178,
                margin: EdgeInsets.only(right: 29, bottom: 18),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      right: 0,
                      bottom: 0,
                      child: Container(
                        width: 349,
                        height: 178,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(110, 165, 105, 149),
                              offset: Offset(0, 1),
                              blurRadius: 4,
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(18)),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 22, top: 22),
                              child: Text(
                                "Workout kicks stress out",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 16,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              width: 209,
                              margin: EdgeInsets.only(left: 22),
                              child: Text(
                                "You exercised 3 days this week - lower than your weekly average. Bust a sweat in the gym today! Tap to see tips.",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 194, 193, 194),
                                  fontSize: 16,
                                  fontFamily: "Roboto",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      right: 35,
                      bottom: 13,
                      child: Container(
                        width: 80,
                        height: 150,
                        child: Image.asset(
                          "assets/images/group-15-2.png",
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      bottom: 0,
                      child: Container(
                        width: 349,
                        height: 178,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(64, 165, 105, 149),
                              offset: Offset(0, 1),
                              blurRadius: 4,
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(18)),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 209,
                              height: 144,
                              margin: EdgeInsets.only(left: 22, top: 22),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Workout kicks stress out",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 66, 33, 61),
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      width: 209,
                                      child: Text(
                                        "You exercised 3 days this week - lower than your weekly average. Bust a sweat in the gym today! Tap to see tips.",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 194, 193, 194),
                                          fontSize: 16,
                                          fontFamily: "Roboto",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                width: 80,
                                height: 150,
                                margin: EdgeInsets.only(right: 35),
                                child: Image.asset(
                                  "assets/images/group-15-2.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      bottom: 0,
                      child: Container(
                        width: 349,
                        height: 178,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(64, 165, 105, 149),
                              offset: Offset(0, 1),
                              blurRadius: 4,
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(14)),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 209,
                              height: 153,
                              margin: EdgeInsets.only(left: 22, top: 25),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Workout kicks stress out",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 66, 33, 61),
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Text(
                                    "You exercised 3 days this week - lower than your weekly average. Bust a sweat in the gym today! Tap to see tips.",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 194, 193, 194),
                                      fontSize: 16,
                                      fontFamily: "Roboto",
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ],
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                width: 80,
                                height: 150,
                                margin: EdgeInsets.only(right: 35),
                                child: Image.asset(
                                  "assets/images/group-15-3.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 67,
              margin: EdgeInsets.only(bottom: 100),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(128, 141, 141, 141),
                    offset: Offset(0, 2),
                    blurRadius: 4,
                  ),
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 54, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 18,
                          child: Text(
                            "HOME",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/home-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 70, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/local-play-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: 21,
                    height: 32,
                    margin: EdgeInsets.only(top: 20, right: 69),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 16,
                          right: 0,
                          child: Text(
                            "CHAT",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 1,
                          child: Container(
                            width: 20,
                            height: 20,
                            child: Image.asset(
                              "assets/images/chat-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 31,
                    height: 30,
                    margin: EdgeInsets.only(top: 22, right: 49),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 14,
                          right: 0,
                          child: Text(
                            "PROFILE",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 9,
                          child: Container(
                            width: 16,
                            height: 16,
                            child: Image.asset(
                              "assets/images/person-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
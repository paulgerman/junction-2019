
import 'package:flutter/material.dart';


class MainWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 531,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    left: 0,
                    top: 0,
                    right: 0,
                    child: Container(
                      height: 531,
                      child: Stack(
                        alignment: Alignment.centerLeft,
                        children: [
                          Positioned(
                            left: 0,
                            right: 3,
                            child: Container(
                              height: 531,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(102, 135, 131, 209),
                                borderRadius: BorderRadius.all(Radius.circular(36)),
                              ),
                              child: Container(),
                            ),
                          ),
                          Positioned(
                            left: 0,
                            top: 82,
                            right: 1,
                            child: Container(
                              height: 300,
                              child: Opacity(
                                opacity: 0.302,
                                child: Image.asset(
                                  "assets/images/path-11.png",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 37,
                            top: 183,
                            child: Container(
                              width: 330,
                              height: 300,
                              child: Image.asset(
                                "assets/images/group-7.png",
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 38,
                    top: 121,
                    child: Container(
                      width: 200,
                      height: 76,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Hi Abby!",
                              style: TextStyle(
                                color: Color.fromARGB(255, 66, 33, 61),
                                fontSize: 36,
                                fontFamily: "Roboto",
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(top: 9),
                              child: Text(
                                "Welcome to your dashboard",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 66, 33, 61),
                                  fontSize: 16,
                                  fontFamily: "Roboto",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 342,
                height: 118,
                margin: EdgeInsets.only(top: 27),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 0,
                      right: 0,
                      child: Container(
                        height: 118,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(66, 66, 33, 61),
                              offset: Offset(0, 1),
                              blurRadius: 5,
                            ),
                          ],
                        ),
                        child: Image.asset(
                          "assets/images/rectangle-7.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 13,
                      top: 6,
                      right: 10,
                      child: Container(
                        height: 91,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 176,
                                height: 91,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 4),
                                        child: Text(
                                          "How do you feel today?",
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 194, 193, 194),
                                            fontSize: 16,
                                            fontFamily: "Roboto",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 176,
                                        height: 45,
                                        margin: EdgeInsets.only(top: 22),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                width: 43,
                                                height: 45,
                                                child: Image.asset(
                                                  "assets/images/group-2-3.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                width: 43,
                                                height: 45,
                                                margin: EdgeInsets.only(left: 23),
                                                child: Image.asset(
                                                  "assets/images/group-3-2.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                width: 43,
                                                height: 45,
                                                margin: EdgeInsets.only(left: 24),
                                                child: Image.asset(
                                                  "assets/images/group-6.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 43,
                                height: 45,
                                margin: EdgeInsets.only(top: 46, right: 23),
                                child: Image.asset(
                                  "assets/images/group-4-2.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 53,
                                height: 82,
                                margin: EdgeInsets.only(top: 9),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: Container(
                                        width: 14,
                                        height: 14,
                                        child: Opacity(
                                          opacity: 0.476,
                                          child: Image.asset(
                                            "assets/images/material-icons-black-close-7.png",
                                            fit: BoxFit.none,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: Container(
                                        width: 43,
                                        height: 45,
                                        margin: EdgeInsets.only(top: 23, right: 10),
                                        child: Image.asset(
                                          "assets/images/group-5-2.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(left: 34, top: 35, right: 38, bottom: 17),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      width: 160,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 160,
                              height: 160,
                              child: Stack(
                                alignment: Alignment.centerRight,
                                children: [
                                  Positioned(
                                    left: 0,
                                    right: 3,
                                    child: Container(
                                      height: 160,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment(0.5, 1),
                                          end: Alignment(0.5, 0),
                                          stops: [
                                            0,
                                            1,
                                          ],
                                          colors: [
                                            Color.fromARGB(255, 162, 97, 144),
                                            Color.fromARGB(255, 104, 50, 87),
                                          ],
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(18)),
                                      ),
                                      child: Container(),
                                    ),
                                  ),
                                  Positioned(
                                    left: 0,
                                    top: 23,
                                    right: 0,
                                    child: Container(
                                      height: 115,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Container(
                                            height: 30,
                                            margin: EdgeInsets.only(left: 18, right: 14),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "Stress",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 255, 255, 255),
                                                      fontSize: 16,
                                                      fontFamily: "Roboto",
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Opacity(
                                                    opacity: 0.4,
                                                    child: Container(
                                                      width: 30,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                        color: Color.fromARGB(255, 255, 255, 255),
                                                        borderRadius: BorderRadius.all(Radius.circular(15)),
                                                      ),
                                                      child: Container(),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 50,
                                            margin: EdgeInsets.only(top: 35),
                                            child: Image.asset(
                                              "assets/images/group-8.png",
                                              fit: BoxFit.none,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top: 31,
                                    right: 20,
                                    child: Container(
                                      width: 18,
                                      height: 16,
                                      child: Image.asset(
                                        "assets/images/material-icons-white-favorite.png",
                                        fit: BoxFit.none,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 157,
                              height: 160,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment(0.5, 1),
                                  end: Alignment(0.5, 0),
                                  stops: [
                                    0,
                                    1,
                                  ],
                                  colors: [
                                    Color.fromARGB(171, 189, 64, 137),
                                    Color.fromARGB(255, 189, 64, 137),
                                  ],
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(18)),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Container(
                                    height: 30,
                                    margin: EdgeInsets.only(left: 18, top: 19, right: 11),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            "Sleep",
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 255, 255, 255),
                                              fontSize: 16,
                                              fontFamily: "Roboto",
                                              fontWeight: FontWeight.w700,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Spacer(),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            width: 30,
                                            height: 30,
                                            child: Stack(
                                              alignment: Alignment.center,
                                              children: [
                                                Positioned(
                                                  top: 0,
                                                  right: 0,
                                                  child: Opacity(
                                                    opacity: 0.4,
                                                    child: Container(
                                                      width: 30,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                        color: Color.fromARGB(255, 255, 255, 255),
                                                        borderRadius: BorderRadius.all(Radius.circular(15)),
                                                      ),
                                                      child: Container(),
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 4,
                                                  right: 4,
                                                  child: Container(
                                                    width: 22,
                                                    height: 22,
                                                    child: Stack(
                                                      alignment: Alignment.center,
                                                      children: [
                                                        Positioned(
                                                          left: 0,
                                                          right: 0,
                                                          child: Container(
                                                            height: 22,
                                                            child: Image.asset(
                                                              "assets/images/bounding-box.png",
                                                              fit: BoxFit.none,
                                                            ),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          left: 1,
                                                          right: 3,
                                                          child: Container(
                                                            height: 19,
                                                            child: Image.asset(
                                                              "assets/images/group.png",
                                                              fit: BoxFit.none,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 3, right: 54),
                                      child: Text(
                                        "6h30m",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 255, 255, 255),
                                          fontSize: 14,
                                          fontFamily: "Roboto",
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Container(
                                      width: 106,
                                      height: 65,
                                      margin: EdgeInsets.only(right: 23),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(top: 6),
                                            child: Opacity(
                                              opacity: 0.854,
                                              child: Container(
                                                width: 4,
                                                height: 59,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 8, top: 17),
                                            child: Opacity(
                                              opacity: 0.854,
                                              child: Container(
                                                width: 4,
                                                height: 48,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 7, top: 6),
                                            child: Opacity(
                                              opacity: 0.854,
                                              child: Container(
                                                width: 4,
                                                height: 59,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(left: 8),
                                              child: Opacity(
                                                opacity: 0.854,
                                                child: Container(
                                                  width: 4,
                                                  height: 65,
                                                  decoration: BoxDecoration(
                                                    color: Color.fromARGB(255, 255, 255, 255),
                                                  ),
                                                  child: Container(),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 7, top: 15),
                                            child: Opacity(
                                              opacity: 0.854,
                                              child: Container(
                                                width: 4,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 7),
                                              child: Opacity(
                                                opacity: 0.3,
                                                child: Container(
                                                  width: 4,
                                                  height: 24,
                                                  decoration: BoxDecoration(
                                                    color: Color.fromARGB(255, 255, 255, 255),
                                                  ),
                                                  child: Container(),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 8),
                                              child: Opacity(
                                                opacity: 0.3,
                                                child: Container(
                                                  width: 4,
                                                  height: 22,
                                                  decoration: BoxDecoration(
                                                    color: Color.fromARGB(255, 255, 255, 255),
                                                  ),
                                                  child: Container(),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 15, right: 7),
                                            child: Opacity(
                                              opacity: 0.854,
                                              child: Container(
                                                width: 4,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 7),
                                              child: Opacity(
                                                opacity: 0.854,
                                                child: Container(
                                                  width: 4,
                                                  height: 61,
                                                  decoration: BoxDecoration(
                                                    color: Color.fromARGB(255, 255, 255, 255),
                                                  ),
                                                  child: Container(),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Opacity(
                                              opacity: 0.334,
                                              child: Container(
                                                width: 4,
                                                height: 18,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 157,
                        height: 340,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: Container(
                                width: 157,
                                height: 340,
                                child: Stack(
                                  alignment: Alignment.centerRight,
                                  children: [
                                    Positioned(
                                      left: 17,
                                      top: 23,
                                      right: 14,
                                      child: Container(
                                        height: 238,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            Container(
                                              height: 48,
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                                children: [
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      width: 77,
                                                      child: Text(
                                                        "Daily Activity",
                                                        style: TextStyle(
                                                          color: Color.fromARGB(255, 255, 255, 255),
                                                          fontSize: 16,
                                                          fontFamily: "Roboto",
                                                          fontWeight: FontWeight.w700,
                                                        ),
                                                        textAlign: TextAlign.left,
                                                      ),
                                                    ),
                                                  ),
                                                  Spacer(),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Opacity(
                                                      opacity: 0.402,
                                                      child: Container(
                                                        width: 30,
                                                        height: 30,
                                                        decoration: BoxDecoration(
                                                          color: Color.fromARGB(255, 255, 255, 255),
                                                          borderRadius: BorderRadius.all(Radius.circular(15)),
                                                        ),
                                                        child: Container(),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topCenter,
                                              child: Container(
                                                margin: EdgeInsets.only(top: 166),
                                                child: Text(
                                                  "7h46m",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(255, 255, 255, 255),
                                                    fontSize: 14,
                                                    fontFamily: "Roboto",
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      left: -1,
                                      top: 116,
                                      right: -1,
                                      child: Container(
                                        height: 133,
                                        child: Image.asset(
                                          "assets/images/path-2.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      left: 26,
                                      top: 150,
                                      right: 26,
                                      child: Container(
                                        height: 59,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topRight,
                                              child: Container(
                                                width: 10,
                                                height: 10,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 151, 148, 215),
                                                  border: Border.all(
                                                    color: Color.fromARGB(255, 255, 253, 253),
                                                    width: 2,
                                                  ),
                                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                width: 10,
                                                height: 10,
                                                margin: EdgeInsets.only(top: 39),
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(255, 151, 148, 215),
                                                  border: Border.all(
                                                    color: Color.fromARGB(255, 255, 253, 253),
                                                    width: 2,
                                                  ),
                                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      top: 27,
                                      right: 18,
                                      child: Container(
                                        width: 22,
                                        height: 23,
                                        child: Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            Positioned(
                                              left: 0,
                                              right: 0,
                                              child: Container(
                                                height: 22,
                                                child: Image.asset(
                                                  "assets/images/bounding-box.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              left: 5,
                                              right: 4,
                                              child: Container(
                                                height: 22,
                                                child: Image.asset(
                                                  "assets/images/group-3.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      left: 0,
                                      right: 0,
                                      child: Container(
                                        height: 340,
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            begin: Alignment(0.5, 1),
                                            end: Alignment(0.5, 0),
                                            stops: [
                                              0,
                                              1,
                                            ],
                                            colors: [
                                              Color.fromARGB(166, 135, 131, 209),
                                              Color.fromARGB(255, 135, 131, 209),
                                            ],
                                          ),
                                          borderRadius: BorderRadius.all(Radius.circular(18)),
                                        ),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Container(
                                              width: 101,
                                              margin: EdgeInsets.only(bottom: 45),
                                              child: Text(
                                                "2 out 6 days of moving achieved",
                                                style: TextStyle(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                  fontSize: 12,
                                                  fontFamily: "Roboto",
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: Container(
                                width: 157,
                                height: 340,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment(0.5, 1),
                                    end: Alignment(0.5, 0),
                                    stops: [
                                      0,
                                      1,
                                    ],
                                    colors: [
                                      Color.fromARGB(166, 135, 131, 209),
                                      Color.fromARGB(255, 135, 131, 209),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(18)),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Container(
                                      height: 48,
                                      margin: EdgeInsets.only(left: 17, top: 23, right: 14),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              width: 77,
                                              child: Text(
                                                "Daily Activity",
                                                style: TextStyle(
                                                  color: Color.fromARGB(255, 255, 255, 255),
                                                  fontSize: 16,
                                                  fontFamily: "Roboto",
                                                  fontWeight: FontWeight.w700,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              width: 30,
                                              height: 30,
                                              decoration: BoxDecoration(
                                                color: Color.fromARGB(103, 255, 255, 255),
                                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                              ),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                                children: [
                                                  Container(
                                                    height: 23,
                                                    margin: EdgeInsets.symmetric(horizontal: 4),
                                                    child: Stack(
                                                      alignment: Alignment.center,
                                                      children: [
                                                        Positioned(
                                                          left: 0,
                                                          right: 0,
                                                          child: Container(
                                                            height: 22,
                                                            child: Image.asset(
                                                              "assets/images/bounding-box.png",
                                                              fit: BoxFit.none,
                                                            ),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          left: 5,
                                                          right: 4,
                                                          child: Container(
                                                            height: 22,
                                                            child: Image.asset(
                                                              "assets/images/group-10.png",
                                                              fit: BoxFit.none,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      height: 145,
                                      margin: EdgeInsets.only(top: 45),
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Positioned(
                                            top: 121,
                                            child: Text(
                                              "7h46m",
                                              style: TextStyle(
                                                color: Color.fromARGB(255, 255, 255, 255),
                                                fontSize: 14,
                                                fontFamily: "Roboto",
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          Positioned(
                                            left: 0,
                                            top: 0,
                                            right: 0,
                                            child: Container(
                                              height: 133,
                                              child: Image.asset(
                                                "assets/images/group-16-2.png",
                                                fit: BoxFit.none,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topCenter,
                                      child: Container(
                                        width: 101,
                                        margin: EdgeInsets.only(bottom: 45),
                                        child: Text(
                                          "2 out 6 days of moving achieved",
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 255, 255, 255),
                                            fontSize: 12,
                                            fontFamily: "Roboto",
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 67,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(128, 141, 141, 141),
                    offset: Offset(0, 2),
                    blurRadius: 4,
                  ),
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 54, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 18,
                          child: Text(
                            "HOME",
                            style: TextStyle(
                              color: Color.fromARGB(255, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/home-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 70, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/local-play-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: 21,
                    height: 32,
                    margin: EdgeInsets.only(top: 20, right: 69),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 16,
                          right: 0,
                          child: Text(
                            "CHAT",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 1,
                          child: Container(
                            width: 20,
                            height: 20,
                            child: Image.asset(
                              "assets/images/chat-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 31,
                    height: 30,
                    margin: EdgeInsets.only(top: 22, right: 49),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 14,
                          right: 0,
                          child: Text(
                            "PROFILE",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 9,
                          child: Container(
                            width: 16,
                            height: 16,
                            child: Image.asset(
                              "assets/images/person-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
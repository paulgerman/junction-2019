import 'dart:io';

import 'package:flutter/material.dart';
import 'package:junction_2019/stores/userStore.dart';
import 'package:oauth2/oauth2.dart' as oauth2;
import 'package:provider/provider.dart';
import 'package:uni_links/uni_links.dart';
import 'package:url_launcher/url_launcher.dart';

import 'extract_token_info.dart';

final authorizationEndpoint =
    Uri.parse("https://www.fitbit.com/oauth2/authorize");
final tokenEndpoint = Uri.parse("https://api.fitbit.com/oauth2/token");
final identifier = "22BDCT";
final secret = "fe81897c962af9a9554df5a24cf9e783";
final redirectUrl = Uri.parse("https://notused");
final _scopes = ['sleep ', 'activity', 'heartrate'];

class LoginPage extends StatefulWidget {
  LoginPage({Key key, @required this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  oauth2.AuthorizationCodeGrant grant;
  oauth2.Client _client;

  Uri _uri;

  @override
  void initState() {
    super.initState();
    grant = new oauth2.AuthorizationCodeGrant(
        identifier, authorizationEndpoint, tokenEndpoint,
        secret: secret);
    _uri = grant.getAuthorizationUrl(redirectUrl, scopes: _scopes);
    initUrlListener();
  }

  // This is used for the callback to the app
  // The url should be registered in AndroidManifest.xml and Info.plist
  initUrlListener() {
    getUriLinksStream().listen((Uri uri) async {
      var client = await grant.handleAuthorizationResponse(uri.queryParameters);
      setState(() {
        _client = client;

        UserStore userStore = Provider.of<UserStore>(context);
        userStore.setAccessToken(accessToken: client.credentials.accessToken);
        Navigator.pushNamed(context, "/main");
      });
    });
  }

  signin() async {
    var url = _uri.toString();

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  check() async {
    if(_client?.credentials?.accessToken != null)
      Navigator.pushNamed(context, "/main");
  }

  @override
  Widget build(BuildContext context) {
    //check();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: RaisedButton(
          child: new Text("Sign in"),
          onPressed: () => {signin()},
        ),
      ),
      body: Center(
        child: Padding(
          child: Column(
            children: <Widget>[
              Text(_client?.credentials?.accessToken ?? 'Not signed in yet'),
              // ExtractTokenInfo(token: _client?.credentials?.accessToken), // Uncomment to display name from claims
            ],
          ),
          padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
        ),
      ),
    );
  }
}

Uri addQueryParameters(Uri url, Map<String, String> parameters) => url.replace(
    queryParameters: new Map.from(url.queryParameters)..addAll(parameters));

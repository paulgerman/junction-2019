
import 'package:flutter/material.dart';


class SleepWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 509,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        height: 509,
                        margin: EdgeInsets.only(right: 11),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Container(
                                height: 509,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment(0.5, 1),
                                    end: Alignment(0.5, 0.052),
                                    stops: [
                                      0,
                                      1,
                                    ],
                                    colors: [
                                      Color.fromARGB(255, 210, 125, 175),
                                      Color.fromARGB(255, 190, 66, 138),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(36)),
                                ),
                                child: Container(),
                              ),
                            ),
                            Positioned(
                              left: 35,
                              top: 105,
                              right: 47,
                              child: Container(
                                height: 383,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 7,
                                        height: 12,
                                        margin: EdgeInsets.only(left: 8),
                                        child: Image.asset(
                                          "assets/images/material-icons-white-chevron-left.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 194,
                                        margin: EdgeInsets.only(left: 7, top: 32),
                                        child: Text(
                                          "Sleep Tracking",
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 255, 255, 255),
                                            fontSize: 36,
                                            fontFamily: "Roboto",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 332,
                                        height: 184,
                                        margin: EdgeInsets.only(top: 34),
                                        child: Stack(
                                          alignment: Alignment.bottomCenter,
                                          children: [
                                            Positioned(
                                              left: 0,
                                              top: 0,
                                              right: 1,
                                              bottom: 0,
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                                children: [
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(top: 17),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 167,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 23, top: 48),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 136,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 20, top: 17),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 167,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 23),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 184,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 20, top: 42),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 142,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Spacer(),
                                                  Align(
                                                    alignment: Alignment.bottomLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(right: 23),
                                                      child: Opacity(
                                                        opacity: 0.3,
                                                        child: Container(
                                                          width: 11,
                                                          height: 62,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(top: 42, right: 20),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 142,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(top: 11, right: 20),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 173,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.bottomLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(right: 20),
                                                      child: Opacity(
                                                        opacity: 0.334,
                                                        child: Container(
                                                          width: 11,
                                                          height: 51,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(top: 11),
                                                      child: Opacity(
                                                        opacity: 0.854,
                                                        child: Container(
                                                          width: 11,
                                                          height: 173,
                                                          decoration: BoxDecoration(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                          ),
                                                          child: Container(),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0,
                                              child: Opacity(
                                                opacity: 0.3,
                                                child: Container(
                                                  width: 11,
                                                  height: 68,
                                                  decoration: BoxDecoration(
                                                    color: Color.fromARGB(255, 255, 255, 255),
                                                  ),
                                                  child: Container(),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 17,
                                      margin: EdgeInsets.only(top: 4, right: 2),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              width: 202,
                                              height: 16,
                                              margin: EdgeInsets.only(top: 1),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "M",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(179, 255, 255, 255),
                                                      fontSize: 12,
                                                      fontFamily: "Roboto",
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 24),
                                                    child: Text(
                                                      "T",
                                                      style: TextStyle(
                                                        color: Color.fromARGB(179, 255, 255, 255),
                                                        fontSize: 12,
                                                        fontFamily: "Roboto",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 20),
                                                    child: Text(
                                                      "W",
                                                      style: TextStyle(
                                                        color: Color.fromARGB(179, 255, 255, 255),
                                                        fontSize: 12,
                                                        fontFamily: "Roboto",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  Spacer(),
                                                  Container(
                                                    margin: EdgeInsets.only(right: 21),
                                                    child: Text(
                                                      "T",
                                                      style: TextStyle(
                                                        color: Color.fromARGB(179, 255, 255, 255),
                                                        fontSize: 12,
                                                        fontFamily: "Roboto",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(right: 24),
                                                    child: Text(
                                                      "F",
                                                      style: TextStyle(
                                                        color: Color.fromARGB(179, 255, 255, 255),
                                                        fontSize: 12,
                                                        fontFamily: "Roboto",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(right: 22),
                                                    child: Text(
                                                      "S",
                                                      style: TextStyle(
                                                        color: Color.fromARGB(179, 255, 255, 255),
                                                        fontSize: 12,
                                                        fontFamily: "Roboto",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  Text(
                                                    "S",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(179, 255, 255, 255),
                                                      fontSize: 12,
                                                      fontFamily: "Roboto",
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 21),
                                              child: Text(
                                                "M",
                                                style: TextStyle(
                                                  color: Color.fromARGB(179, 255, 255, 255),
                                                  fontSize: 12,
                                                  fontFamily: "Roboto",
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 23),
                                              child: Text(
                                                "T",
                                                style: TextStyle(
                                                  color: Color.fromARGB(179, 255, 255, 255),
                                                  fontSize: 12,
                                                  fontFamily: "Roboto",
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 21),
                                              child: Text(
                                                "W",
                                                style: TextStyle(
                                                  color: Color.fromARGB(179, 255, 255, 255),
                                                  fontSize: 12,
                                                  fontFamily: "Roboto",
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              "T",
                                              style: TextStyle(
                                                color: Color.fromARGB(179, 255, 255, 255),
                                                fontSize: 12,
                                                fontFamily: "Roboto",
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.only(top: 471, right: 22),
                      child: Text(
                        "S",
                        style: TextStyle(
                          color: Color.fromARGB(179, 255, 255, 255),
                          fontSize: 12,
                          fontFamily: "Roboto",
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.only(top: 471),
                      child: Text(
                        "S",
                        style: TextStyle(
                          color: Color.fromARGB(179, 255, 255, 255),
                          fontSize: 12,
                          fontFamily: "Roboto",
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 35, top: 33),
                child: Text(
                  "Week 40",
                  style: TextStyle(
                    color: Color.fromARGB(255, 104, 50, 87),
                    fontSize: 16,
                    fontFamily: "Roboto",
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 346,
                height: 43,
                margin: EdgeInsets.only(top: 15),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      width: 250,
                      margin: EdgeInsets.only(bottom: 4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 146,
                              height: 24,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "21.8",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 194, 193, 194),
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 30),
                                      child: Text(
                                        "23.06 - 7.30",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 194, 193, 194),
                                          fontSize: 16,
                                          fontFamily: "Roboto",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 250,
                              height: 7,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 156, 93, 139),
                                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                              ),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Text(
                          "8h 25m",
                          style: TextStyle(
                            color: Color.fromARGB(255, 194, 193, 194),
                            fontSize: 16,
                            fontFamily: "Roboto",
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 346,
                height: 43,
                margin: EdgeInsets.only(top: 23),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      width: 222,
                      margin: EdgeInsets.only(bottom: 4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 146,
                              height: 24,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "20.8",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 194, 193, 194),
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 30),
                                      child: Text(
                                        "22.06 - 7.30",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 194, 193, 194),
                                          fontSize: 16,
                                          fontFamily: "Roboto",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 222,
                              height: 7,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 156, 93, 139),
                                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                              ),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Text(
                          "7h 25m",
                          style: TextStyle(
                            color: Color.fromARGB(255, 194, 193, 194),
                            fontSize: 16,
                            fontFamily: "Roboto",
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 346,
                height: 43,
                margin: EdgeInsets.only(bottom: 23),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      width: 280,
                      margin: EdgeInsets.only(bottom: 4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 146,
                              height: 24,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "19.8",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 194, 193, 194),
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 30),
                                      child: Text(
                                        "21.06 - 8.30",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 194, 193, 194),
                                          fontSize: 16,
                                          fontFamily: "Roboto",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 280,
                              height: 7,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 156, 93, 139),
                                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                              ),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Text(
                          "9h 25m",
                          style: TextStyle(
                            color: Color.fromARGB(255, 194, 193, 194),
                            fontSize: 16,
                            fontFamily: "Roboto",
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 346,
                height: 43,
                margin: EdgeInsets.only(bottom: 53),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      width: 146,
                      margin: EdgeInsets.only(bottom: 4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 146,
                              height: 24,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "18.8",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 194, 193, 194),
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 30),
                                      child: Text(
                                        "23.58 - 5.30",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 194, 193, 194),
                                          fontSize: 16,
                                          fontFamily: "Roboto",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 145,
                              height: 7,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 156, 93, 139),
                                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                              ),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Text(
                          "5h 25m",
                          style: TextStyle(
                            color: Color.fromARGB(255, 194, 193, 194),
                            fontSize: 16,
                            fontFamily: "Roboto",
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 67,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(128, 141, 141, 141),
                    offset: Offset(0, 2),
                    blurRadius: 4,
                  ),
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 54, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 18,
                          child: Text(
                            "HOME",
                            style: TextStyle(
                              color: Color.fromARGB(255, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/home-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 24,
                    height: 34,
                    margin: EdgeInsets.only(left: 70, top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 3,
                          top: 18,
                          child: Text(
                            "FEED",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Image.asset(
                              "assets/images/local-play-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: 21,
                    height: 32,
                    margin: EdgeInsets.only(top: 20, right: 69),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 16,
                          right: 0,
                          child: Text(
                            "CHAT",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 1,
                          child: Container(
                            width: 20,
                            height: 20,
                            child: Image.asset(
                              "assets/images/chat-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 31,
                    height: 30,
                    margin: EdgeInsets.only(top: 22, right: 49),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 14,
                          right: 0,
                          child: Text(
                            "PROFILE",
                            style: TextStyle(
                              color: Color.fromARGB(179, 66, 33, 61),
                              fontSize: 8,
                              fontFamily: "Roboto",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 9,
                          child: Container(
                            width: 16,
                            height: 16,
                            child: Image.asset(
                              "assets/images/person-24px.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
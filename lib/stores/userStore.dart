import 'dart:async';
import 'dart:convert';

import 'package:mobx/mobx.dart';

import 'package:flutter/foundation.dart';


part 'userStore.g.dart';

class UserStore = _UserStore with _$UserStore;

abstract class _UserStore with Store {
  String accessToken;

  @observable
  String userName;

  @computed
  bool get isLoggedIn => accessToken != null && accessToken.length != 0;

  @action
  Future<void> setAccessToken({@required String accessToken}) async {
    this.accessToken = accessToken;
  }
  @action
  Future<void> setUserName({@required String userName}) async {
    this.userName = userName;
  }

  Future<void> _setup() async {

  }
}

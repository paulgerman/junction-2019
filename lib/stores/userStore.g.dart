// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserStore on _UserStore, Store {
  Computed<bool> _$isLoggedInComputed;

  @override
  bool get isLoggedIn =>
      (_$isLoggedInComputed ??= Computed<bool>(() => super.isLoggedIn)).value;

  final _$userNameAtom = Atom(name: '_UserStore.userName');

  @override
  String get userName {
    _$userNameAtom.context.enforceReadPolicy(_$userNameAtom);
    _$userNameAtom.reportObserved();
    return super.userName;
  }

  @override
  set userName(String value) {
    _$userNameAtom.context.conditionallyRunInAction(() {
      super.userName = value;
      _$userNameAtom.reportChanged();
    }, _$userNameAtom, name: '${_$userNameAtom.name}_set');
  }

  final _$setAccessTokenAsyncAction = AsyncAction('setAccessToken');

  @override
  Future<void> setAccessToken({@required String accessToken}) {
    return _$setAccessTokenAsyncAction
        .run(() => super.setAccessToken(accessToken: accessToken));
  }
}
